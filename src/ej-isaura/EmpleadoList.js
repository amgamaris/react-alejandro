import React, {Component} from 'react';
import EmpleadoRow from './EmpleadoRow';

export default class EmpleadoList extends Component{
    render(){
        return(
            <div className="contenido">
                <article className="empleados">
                    {
                    this.props.empleados.map((element,i)=>
                    <EmpleadoRow data={element} key={i}/>)
                    }
                </article>
            </div>
        );
    }
}