import React, {Component} from 'react';
import EmpleadoAvatar from './EmpleadoAvatar';
import './empleado.css';

export default class EmpleadoRow extends Component{
    render(){
        let empleado=this.props.data;
        return(
            <section className="empleado">
                <EmpleadoAvatar avatar={empleado.avatar}></EmpleadoAvatar>
                <div className="info">
                    <p>{empleado.nombre}</p>
                    <div className="more-info">
                        <p>{empleado.puesto}</p>
                        <p>{empleado.departamento}</p>
                    </div>
                </div>
            </section>
        );
    }
}