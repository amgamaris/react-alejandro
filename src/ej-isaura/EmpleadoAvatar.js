import React, {Component} from 'react';

export default class EmpleadoAvatar extends Component{
    render(){
        return(
            <img className="avatar" src={this.props.avatar}/>
        );
    }
}