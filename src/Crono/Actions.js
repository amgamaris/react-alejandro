import React, {Component} from 'react';
import './crono.css'

export default class Actions extends Component{
    render(){
        return(
            <div className="actions">
                <button onClick={this.props.onClick1}>STOP</button>
                <button onClick={this.props.onClick2}>START</button>
            </div>
        );
    }
}