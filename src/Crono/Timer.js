import React, {Component} from 'react';
import './crono.css'
import Actions from './Actions.js'

export default class Timer extends Component{
    constructor(){
        super();
        this.state={
            hours: 0,
            minutes: 0,
            seconds: 0,
            mseconds:0,
            interval: null,
            isRunning: false
        }
        this.startClick=this.startClick.bind(this);
        this.stopClick=this.stopClick.bind(this);
        this.counter=this.counter.bind(this);
    }

    startClick(){ //Iniciar cronometro
        if(!this.state.isRunning){
            this.setState({interval: setInterval(this.counter,100) });
            this.setState({isRunning: true});
        }
    }

    stopClick(){ //Parar cronometro con un toque y reiniciar con dos
        if(this.state.isRunning){
            clearInterval(this.state.interval);
            this.setState({isRunning: false});
        }
        else{
            this.setState({hours:0, minutes: 0, seconds: 0, mseconds:0});
        }
    }

    counter(){ //Contador del tiempo
        this.setState({mseconds:this.state.mseconds+1});  
        if(this.state.mseconds===10){
            this.setState({mseconds: 0});
            this.setState({seconds: this.state.seconds+1});
        }                  
        if(this.state.seconds===60){
            this.setState({seconds: 0});
            this.setState({minutes: this.state.minutes+1});
        }           
        if(this.state.minutes===60){
            this.setState({minutes: 0});
            this.setState({hours: this.state.hours+1});
        }                    
    }

    timeConvers1(n){ 
        return (n<10) ? '0'+n : ''+n;
    }

    timeConvers2(n){
        return (n<100) ? this.timeConvers1(n) : n;
    }

    render(){
        return(
            <div className="content">
                <div className="timer">
                    <span className="timer-hours">{this.timeConvers2(this.state.hours)}</span>:
                    <span className="timer-minutes">{this.timeConvers2(this.state.minutes)}</span>:
                    <span className="timer-seconds">{this.timeConvers2(this.state.seconds)}</span>
                </div>
                <Actions onClick1={this.stopClick} onClick2={this.startClick}></Actions>
            </div>
        );
    }
}