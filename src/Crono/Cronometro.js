import React, {Component} from 'react';
import './crono.css'
import Timer from './Timer.js';


export default class Cronometro extends Component{
    render(){
        return(
                <div className="header">
                    <h2>{this.props.title}</h2>
                    <Timer></Timer>
                </div>
        );
    }
}
