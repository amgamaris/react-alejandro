import React, { Component } from "react";

// const style={
//   color: 'coral',
//   fontFamily: 'Arial',
//   textDecoration: 'underline'
// };

const ListItem = () => <div>Hola</div>

export default class HolaMundo extends Component{
  constructor(){
    super();
    this.items=[];
    for(let i=0;i<10;i++){
      this.items.push(i);
    }
  }

  render() {
    return (
      <div>
        {this.items.map(i=><ListItem key={i}/>)}
        {this.props.name}
      </div>
    );
  }
}