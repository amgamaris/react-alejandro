import React, {Component} from 'react';

export default class Coche extends Component{
    constructor(){
        super();
        this.state={
            count: 0,
            face: ":("
        };
        this.goClick=this.clicking.bind(this);
        this.face=":(";
    }
    clicking(){
        if(this.state.count+1>=5){
            this.setState({count: this.state.count+1, face: ":D"});
        }
        else{        
            this.setState({count: this.state.count+1, face: ":)"});
        }
    }
    render(){
        return(
            <div>
                <p>Me has clickado {this.state.count} veces  <b>{this.state.face}</b></p>
                <button onClick={this.goClick}>CLICK ME</button>
            </div>
        );
    }
}