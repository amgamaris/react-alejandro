import React, { Component } from "react";
import DateItem from "./DateItem.js"

const TimeZone=[
    {country:'España', difUTC: 0},
    {country:'Estados Unidos', difUTC: -4},
    {country:'China', difUTC: +4}
];

const MoveDate = (date, difUTC) => {
    var dateMoved=new Date(date);
    dateMoved.setUTCHours(dateMoved.getUTCHours()+difUTC);
    return dateMoved;
};

export default class DateWorld extends Component{

    render(){
        let now=new Date();
        let dates= TimeZone.map(zone=>
            <DateItem key={zone.country} country={zone.country} date={MoveDate(now, zone.difUTC)}></DateItem>
            );
        return (<div><h1>Fechas del mundo</h1>{dates}</div>)
    }

}