import React, { Component } from "react";
import PropTypes from 'prop-types';

export default class DateItem extends Component{

    render(){
        const {country, date}=this.props;
        return(<p>En {country} son las {date.toTimeString()}.</p>);
    };
};

DateItem.defaultProps={
    country: 'España',
    date: new Date()
}

DateItem.propTypes ={
    country: PropTypes.string.isRequired,
    date: PropTypes.object.isRequired
}