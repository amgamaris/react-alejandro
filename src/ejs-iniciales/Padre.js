import React, {Component} from 'react';
import Button from './Button.js';

export default class Padre extends Component{
    constructor(){
        super();
        this.handleStart=this.handleStart.bind(this);
        this.handleStop=this.handleStop.bind(this);
    }

    handleStart(){
        console.log("Start");
    }
    handleStop(){
        console.log("Stop");
    }

    render(){
        return(
            <div>
                <Button onStart={this.handleStart} onStop={this.handleStop}></Button>
            </div>
        );
    }
}