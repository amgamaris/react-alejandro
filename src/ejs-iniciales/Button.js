import React, {Component} from 'react';

export default class Button extends Component{

    render(){
        return(
            <div>
                <button onClick={this.props.onStart}>START</button>
                <button onClick={this.props.onStop}>STOP</button>
            </div>
        );
    }
}