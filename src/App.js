import React, {Component} from 'react';
// import Cronometro from './Crono/Cronometro.js';
// import EmpleadoList from './ej-isaura/EmpleadoList.js';
import Formulario from './Formulario.js';

export default class App extends Component{
    render(){
        const e=[
            {nombre: "Laya Dueñas", avatar:"girl.png", puesto:"CEO", departamento:"Business"},
            {nombre: "Astryd Vallés", avatar:"girl.png", puesto:"CMO", departamento:"Marketing"},
            {nombre: "Shantell Meza", avatar:"girl.png", puesto:"CFO", departamento:"Business"},
            {nombre: "Sergio Ocampo", avatar:"boy.png", puesto:"CTO", departamento:"Engineering"},
            {nombre: "Paula Patton", avatar:"girl.png", puesto:"Back-end", departamento:"Marketing"},
            {nombre: "Ares Jiménez", avatar:"boy.png", puesto:"Art Director", departamento:"Marketing"},
            {nombre: "Alejandro Mena", avatar:"boy.png", puesto:"Front-end", departamento:"Marketing"},
            {nombre: "Laura Smith", avatar:"girl.png", puesto:"Full-stack", departamento:"Marketing"}
        ]        
        return (
            // <div className="crono">
            //     <Cronometro title="Cronómetro"></Cronometro>
            // </div>
            // <EmpleadoList empleados={e}></EmpleadoList>
            <Formulario></Formulario>
        );
    }
}